#!/usr/bin/python
import validators
import requests
import argparse
import os
import sys


def validate_path(path):
    if path == "-":
        return
    if os.path.isfile(path):
        return path
    else:
        raise argparse.ArgumentTypeError(f"Path '{path}' is not a valid file.")


def validate_uuid(uuid):
    if validators.uuid(uuid):
        return str(uuid)
    else:
        raise argparse.ArgumentTypeError(f"Uuid '{uuid}' is not valid.")


def validate_url(url):
    if validators.url(url):
        return str(url)
    else:
        raise argparse.ArgumentTypeError(f"Url '{url}' is not valid.")


def parse_script_args(args):
    parser = argparse.ArgumentParser(description="CLI application which retrieves and prints data from the described backend.")
    parser.add_argument("--base-url", default="http://localhost/", type=validate_url, help="Show help message and exit.")
    parser.add_argument("--output", default="-", type=validate_path, help="Set the file where to store the output. Default is -, i.e. the stdout.")
    parser.add_argument("subcommands", choices=["stat", "read"], help="stat Prints the file metadata in a human-readable manner.read Outputs the file content.")
    parser.add_argument("uuid", type=validate_uuid)
    parsed_args = parser.parse_args(args)

    if parsed_args.subcommands == "stat":
        stat_requester(parsed_args)
    else:
        read_requester(parsed_args)


def requester(args):
    url = args.base_url + "file/" + args.uuid + ("/stat/" if args.subcommands == "stat" else "/read/")
    response = requests.get(url, timeout=2.50)
    if response.status_code == 404:
        raise argparse.ArgumentTypeError("File is not found.")
    return response


def stat_requester(args):
    json_response = requester(args).json()

    content = "Name of the file: " + str(json_response["name"]) + ".\n" + \
              "File size in bytes: " + str(json_response["size"]) + ".\n" + \
              "Type of file: " + str(json_response["mimetype"]) + ".\n" + \
              "File creation date: " + str(json_response["create_datetime"]) + "."
    print(content)


def read_requester(args):
    response = requester(args)

    if args.output is None:
        print(response.content.decode('utf-8'))
    else:
        with open(args.output, "w") as file:
            file.write(response.content.decode('utf-8'))


def main():
    parse_script_args(sys.argv[1:])


if __name__ == '__main__':
    main()
