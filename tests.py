import unittest
import fileClient
import argparse
import requests
import sys
from io import StringIO

#testing api on adress https://www.mockable.io/a/#/space/demo5505079/rest, but users need to be added
class MyTestCase(unittest.TestCase):
    def test_validate_path(self):
        self.assertEqual(fileClient.validate_path("/Users/Honza/testfile.txt"), "/Users/Honza/testfile.txt")
        self.assertEqual(fileClient.validate_path("/Users/Honza/testfile.txt"), "/Users/Honza/testfile.txt")
        self.assertIsNone(fileClient.validate_path("-"))

    def test_validate_uuid(self):
        self.assertEqual(fileClient.validate_uuid("123e4567-e89b-12d3-a456-426614174009"),
                         "123e4567-e89b-12d3-a456-426614174009")
        self.assertEqual(
            fileClient.validate_uuid("dbc81953-9801-4a82-bca9-a9bae142fd77"),
            "dbc81953-9801-4a82-bca9-a9bae142fd77")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_uuid("123e45677e89b-12d3-a456-426614174009")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_uuid("123e456-e89b-12d3-a456-426614174009")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_uuid("123ek56-e89b-12d3-a456-426614174009")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_uuid("123e@56-e89b-12d3-a456-426614174009")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_uuid("123e/56-e89b-12d3-a456-426614174009")

    def test_validate_url(self):
        self.assertEqual(
            fileClient.validate_url("https://websitebuilders.com"),
            "https://websitebuilders.com")
        self.assertEqual(
            fileClient.validate_url("http://websitebuilders.com"),
            "http://websitebuilders.com")
        self.assertEqual(
            fileClient.validate_url("ftp://aeneas.mit.edu/"),
            "ftp://aeneas.mit.edu/")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_url("htp://websitebuilders.com")
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.validate_url("http://websitebuilders.com.")

    def test_requester(self):
        class Namespace:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)
        args = Namespace(base_url='http://demo5505079.mockable.io/', output=None, subcommands='stat', uuid='123e4567-e89b-12d3-a456-426614174009')
        self.assertTrue(fileClient.requester(args).__eq__(requests.get('http://demo5505079.mockable.io/file/123e4567-e89b-12d3-a456-426614174009/stat/')))

        args = Namespace(base_url='http://demo5505079.mockable.io/', output=None, subcommands='read', uuid='123e4567-e89b-12d3-a456-426614174009')
        self.assertTrue(fileClient.requester(args).__eq__(requests.get('http://demo5505079.mockable.io/file/123e4567-e89b-12d3-a456-426614174009/read/')))

        args = Namespace(base_url='http://demo5079.mockable.io/', output=None, subcommands='stat', uuid='123e4567-e89b-12d3-a456-426614174009')
        with self.assertRaises(argparse.ArgumentTypeError):
            fileClient.requester(args)

    def test_stat_requester(self):
        result = "Name of the file: myTextFile.\n" \
                 "File size in bytes: 300.\n" + \
                  "Type of file: text/plain.\n" + \
                  "File creation date: 2020-07-10 15:00:00.000."

        class Namespace:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

        args = Namespace(base_url='http://demo5505079.mockable.io/',
                         output=None, subcommands='stat',
                         uuid='123e4567-e89b-12d3-a456-426614174009')
        out = StringIO()
        sys.stdout = out
        fileClient.stat_requester(args)
        output = out.getvalue().strip()
        self.assertEqual(result, output)

    def test_read_requester_stdout(self):
        result = "Lorem ipsum dolor sit amet,\nconsectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,\nnisi ut aliquid ex ea commodi consequatur.\nQuis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint obcaecat cupiditat non proident,\nsunt in culpa qui officia deserunt mollit anim id est laborum.ontaining Lorem Ipsum passages,\nand more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

        class Namespace:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)
        args = Namespace(base_url='http://demo5505079.mockable.io/',
                         output=None, subcommands='read',
                         uuid='123e4567-e89b-12d3-a456-426614174009')
        out = StringIO()
        sys.stdout = out
        fileClient.read_requester(args)
        output = out.getvalue().strip()
        self.assertEqual(result, output)

    def test_read_requester_output(self):
        result = "Lorem ipsum dolor sit amet,\nconsectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,\nnisi ut aliquid ex ea commodi consequatur.\nQuis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint obcaecat cupiditat non proident,\nsunt in culpa qui officia deserunt mollit anim id est laborum.ontaining Lorem Ipsum passages,\nand more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

        class Namespace:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)
        args = Namespace(base_url='http://demo5505079.mockable.io/',
                         output="/Users/Honza/testfile.txt", subcommands='read',
                         uuid='123e4567-e89b-12d3-a456-426614174009')

        fileClient.read_requester(args)
        with open(args.output, "r") as file:
            output = file.read()
        self.assertEqual(result, output)

    def test_parse_script_args(self):
        result = "Lorem ipsum dolor sit amet,\nconsectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,\nnisi ut aliquid ex ea commodi consequatur.\nQuis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint obcaecat cupiditat non proident,\nsunt in culpa qui officia deserunt mollit anim id est laborum.ontaining Lorem Ipsum passages,\nand more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        args = ['--base-url=http://demo5505079.mockable.io/',"--output=/Users/Honza/testfile1.txt" , 'read', '123e4567-e89b-12d3-a456-426614174009']
        fileClient.parse_script_args(args)
        with open("/Users/Honza/testfile1.txt", "r") as file:
            output = file.read()
        self.assertEqual(result, output)

        args = ['--base-url=http://demo5505079.mockable.io/', 'read',
                '123e4567-e89b-12d3-a456-426614174009']
        out = StringIO()
        sys.stdout = out
        fileClient.parse_script_args(args)
        output = out.getvalue().strip()
        self.assertEqual(result, output)

        result = "Name of the file: myTextFile.\n" \
                 "File size in bytes: 300.\n" + \
                 "Type of file: text/plain.\n" + \
                 "File creation date: 2020-07-10 15:00:00.000."
        args = ['--base-url=http://demo5505079.mockable.io/', 'stat',
                '123e4567-e89b-12d3-a456-426614174009']
        out = StringIO()
        sys.stdout = out
        fileClient.parse_script_args(args)
        output = out.getvalue().strip()
        self.assertEqual(result, output)


if __name__ == '__main__':
    unittest.main()
